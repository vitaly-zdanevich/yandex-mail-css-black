<div align='center'>
    <a href='https://gitlab.com/vitaly-zdanevich-styles/yandex-mail/-/raw/master/yandex-mail-css-black.user.css' alt='Install with Stylus'>
        <img src='https://img.shields.io/badge/Install%20directly%20with-Stylus-116b59.svg?longCache=true&style=flat' />
    </a>
</div>

![screenshot](/screenshot.png)

Black night AMOLED theme.

Without ad.

Without wasted space (margins/passings).

Without "Services" dropdown at top-right.

Removed "Unsubscribe" button.

Email body: use space at the top

Remove quick reply button.

Without bottom-left buttons to other Yandex services

Without translation button help

For use with [Stylus](https://github.com/openstyles/stylus) for [Chrome](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne) or [Firefox](https://addons.mozilla.org/firefox/addon/styl-us).

Install theme from https://userstyles.world/style/14787/mail-yandex-ru-feb-2024

Discussion page https://yandexmail.userecho.com/communities/4/topics/2101-userstyle-ya-sdelal-normalnyij-dizajn-tma-skryivaet-nedostatki-ona-darit-nam-lyubov-i-mesta-bolshe
